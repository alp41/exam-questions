<?php
header('Content-Type: text/html; charset=utf-8');
$date = date("d");
if($date > 10) {
	echo "<h1 style='text-align:center;'>Tut mir leid, Submission Time ist vorbei!</h1>";
	die();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Was willst du für die Klausur wissen?</title>
		<style>
			.center {
				text-align: center;
			}
			textarea, button {
				display: block;
    			margin-left: auto;
    			margin-right: auto;
			}
			#footer {
				position: absolute;
				bottom: 5px;
				left: 5px;
			}
		</style>
		<script
		src="https://code.jquery.com/jquery-3.6.0.min.js"
		integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
		crossorigin="anonymous"></script>	
	</head>
	<body>
		<h1 class="center">
			Was möchtest du nochmal besprechen?
		</h1>
		<h4 class="center">(What would you like to talk about again?)</h4>
		<textarea id="questions" rows="10" cols="100" placeholder="- Frage 1&#10;- Frage 2&#10;- etc"></textarea>
		<p style="font-size:small" class="center">(Bitte eine Frage pro Zeile)</p>
		<p style="font-size:small" class="center">(Please only one question per line)</p>
		<button id="submit" style="font-size: medium;" class="center" type="submit">Absenden (Submit)</button>
		<p class="center" id="result"></p>
		<h3 class="center">Bisher gestellte Fragen</h3>
		<ul class="center">
		<?php
			$server = "";
			$username = "";
			$password = "";
			$database = "";

			$conn = new mysqli($server, $username, $password, $database);

			if($conn->connect_error) {
				die("err");
			}

			$result = $conn->query("SELECT * FROM exam");
			while($row = $result->fetch_assoc()) {
				echo "<li>".$row["question"]."</li>";
			}
		?>
		</ul>
		<a id="footer" href="https://gitlab.com/alp41/exam-questions" target="_blank">Quellcode</a>
	</body>
	<script>
		$(document).ready(() => {
			$('#submit').click(() => {
				let questions = $('#questions').val();
				$.post("./question_submit.php", {
					questions: questions
				}, (data) => {
					if(data === "err") {}//shrug
					if(data === "1") {
						$('#questions').val("");
						$('#result').text("Abgesendet!");
						setTimeout(() => {
							$('#result').text("");
						}, 2000);

					}
				})
			})
		})
	</script>
</html>